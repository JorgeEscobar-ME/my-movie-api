from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends, APIRouter
from pydantic import BaseModel, Field
from fastapi.responses import HTMLResponse, JSONResponse
from utils.jwtmanager import create_token, validate_token

class User(BaseModel):
    email: str
    password: str
    