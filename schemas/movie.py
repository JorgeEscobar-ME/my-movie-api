
from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends, APIRouter
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.security.http import HTTPAuthorizationCredentials
from pydantic import BaseModel, Field
from typing import Optional, List
from utils.jwtmanager import create_token, validate_token
from fastapi.security import HTTPBearer
from config.database import Session, engine, Base
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.error_handler import ErrorHandler
from middlewares.jwtbearer import JWTBearer




class Movie(BaseModel):
    id: Optional[int] = None #el id puede ser de tipo entero o un None por defecto
    title: str = Field(max_length=50, min_length=5)
    overview: str = Field(max_length=50, min_length=15)
    year: int = Field(le=2022)
    rating: float = Field(ge=0.0, le=10.0)
    category: str = Field(max_length=15, min_length=5)

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "title": "Example Title",
                "overview": "Two imprisoned men bond over a number of years.",
                "year": 2022,
                "rating": 8.3,
                "category": "Drama"
            }
        }
    
