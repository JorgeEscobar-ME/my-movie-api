from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends, APIRouter
from pydantic import BaseModel, Field
from fastapi.responses import HTMLResponse, JSONResponse
from utils.jwtmanager import create_token, validate_token
from schemas.auth import User

auth_router = APIRouter()

@auth_router.post("/login", tags=["auth"])
def login(user: User):
    if user.email == "admin@gmail.com" and user.password=="admin":
        token: str = create_token(user.dict())
    return JSONResponse(token, status_code=200)
