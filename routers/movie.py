
from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends, APIRouter
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.security.http import HTTPAuthorizationCredentials
from pydantic import BaseModel, Field
from typing import Optional, List
from utils.jwtmanager import create_token, validate_token
from fastapi.security import HTTPBearer
from config.database import Session, engine, Base
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.error_handler import ErrorHandler
from middlewares.jwtbearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie


movie_router = APIRouter()


@movie_router.get("/movies", tags=["movies"], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer)]) 
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get("/movies/{id}", tags=["movies"], response_model=Movie)
def get_movie(id: int = Path(ge=1, le=200)) -> Movie :
    db=Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Movie not found"})
    return JSONResponse(content=jsonable_encoder(result), status_code=200 )

@movie_router.get("/movies/", tags=["movies"], response_model=List[Movie])
def get_movies_by_category_and_year(category: str = Query(min_length=5, max_length=15), year: int = Query(le=2022, ge=1800)) -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movie_by_category_and_year(category, year)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Movie not found"})
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

@movie_router.post("/movies/", tags=["movies"], response_model=dict, status_code=201)
def create_movie(movie: Movie) -> dict:
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse({"Message" : "Se ha registrado la pelicula"}, status_code=201)

@movie_router.put("/movies/{id}", tags=["movies"], response_model=dict, status_code=200)
def modify_movie(id: int, movie: Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Movie not found"})
    MovieService(db).modify_movie(id, movie)
    db.commit()
    return JSONResponse({"Message" : "Se ha modificado la pelicula"}, status_code=200)
        
@movie_router.delete("/movies/{id}", tags=["movies"], response_model=dict, status_code=200)
def delete_movie(id: int) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Movie not found"})
    MovieService(db).delete_movie(id)
    db.commit()
    return JSONResponse({"Message" : "Se ha eliminado la pelicula"}, status_code=200)